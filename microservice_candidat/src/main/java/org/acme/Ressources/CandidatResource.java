package org.acme.Ressources;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response;

import org.acme.Entity.Candidat;
import org.acme.Entity.CandidatureDto;
import org.bson.types.ObjectId;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;



@Path("/candidat")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CandidatResource {

    @Inject
    CandidatRepository candidatureRepository;

    @POST
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response AddCandidat(Candidat candidat) {
    
        // Vérifie si le candidat existe déjà
        Candidat candidatExistante = candidatureRepository.find("name = ?1 AND firstname = ?2 AND nationality = ?3 AND dateOfBirth = ?4", 
            candidat.getName(), candidat.getFirstname(), candidat.getNationality(), candidat.getDateOfBirth()).firstResult();
        
        if (candidatExistante != null) {
            return Response.status(Response.Status.CONFLICT).entity("Candidat existant déjà").build();
        }
    
        Candidat candidature = new Candidat();
        candidature.setName(candidat.getName());
        candidature.setFirstname(candidat.getFirstname());
        candidature.setNationality(candidat.getNationality());
        candidature.setDateOfBirth(candidat.getDateOfBirth());
        
        candidatureRepository.persist(candidature);
    
        return Response.created(URI.create("/candidat/" + candidature.getId())).build();
    }
    



//retourne toutes les candidatures/candidas existantes
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Candidat> getAllCandidats() {
        return candidatureRepository.listAll();
    }
    @GET
    @Path("/{id}")
    public Response getCandidature(@PathParam("id") ObjectId id) {
        Optional<Candidat> candidatureOptional = candidatureRepository.findByIdOptional(id);
    
        return candidatureOptional.map(candidature -> {
            CandidatureDto candidatureDTO = new CandidatureDto();
            candidatureDTO.setName(candidature.getName());
            candidatureDTO.setFirstname(candidature.getFirstname());
            candidatureDTO.setDateOfBirth(candidature.getDateOfBirth());
            candidatureDTO.setNationality(candidature.getNationality());
    
            return Response.ok(candidatureDTO).build();
        }).orElse(Response.status(Response.Status.NOT_FOUND).build());
    }
    
/* permet de modifier une candidature selon son id*/
@PUT
@Path("/{id}")
@Transactional
public Response updateCandidature(@PathParam("id") ObjectId id, @Valid CandidatureDto candidatureDto) {
    Candidat candidature = candidatureRepository.findById(id);

    if (candidature == null) {
        throw new NotFoundException("Candidat avec l'identifiant " + id + " n'existe pas.");
    }

    if (candidatureDto.getName() != null) {
        candidature.setName(candidatureDto.getName());
    }
    if (candidatureDto.getFirstname() != null) {
        candidature.setFirstname(candidatureDto.getFirstname());
    }
    if (candidatureDto.getDateOfBirth() != null) {
        candidature.setDateOfBirth(candidatureDto.getDateOfBirth());
    }
    if (candidatureDto.getNationality() != null) {
        candidature.setNationality(candidatureDto.getNationality());
    }

    candidatureRepository.persist(candidature);

    return Response.ok(candidature).build();
}



/* supprimer une candidat en fonction de son ID */
@DELETE
@Path("/{idCandidat}")
@Transactional
public Response deleteCandidature(@PathParam("idCandidat") String idCandidat) {
    Candidat candidat = candidatureRepository.findById(new ObjectId(idCandidat));

    if (candidat == null) {
        throw new NotFoundException("Le candidat avec l'ID " + idCandidat +" n'existe pas.");
    }

    candidatureRepository.delete(candidat);

    return Response.noContent().build();
}



}
