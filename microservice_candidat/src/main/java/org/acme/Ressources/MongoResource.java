package org.acme.Ressources;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;

import org.acme.Entity.Candidat;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

@ApplicationScoped
public class MongoResource {

    private MongoClient mongoClient;
    private MongoDatabase database;

    @ConfigProperty(name = "mongodb+srv://djouher2:RvZiy8LXGHwIrFte@cluster0.vvnw2n5.mongodb.net/microcandidat")
    String connectionString;

    @ConfigProperty(name = "microcandidat")
    String databaseName;

    @PostConstruct
    void init() {
        ConnectionString connString = new ConnectionString(connectionString);
        mongoClient = MongoClients.create(connString);
        database = mongoClient.getDatabase(databaseName);
    }

    public MongoCollection<Candidat> getCollection(String collectionName) {
        return database.getCollection(collectionName, Candidat.class);
    }

    @PreDestroy
    void destroy() {
        mongoClient.close();
    }
}
