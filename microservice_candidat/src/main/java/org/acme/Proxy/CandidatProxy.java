package org.acme.Proxy;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.acme.Entity.Candidat;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

//c'est coté consommateur qu'on met un proxy

//on met dans propreties de l'autre microservice quarkus.http.port=8081
@RegisterRestClient(baseUri = "http://localhost/8081")//c'est le port de l'autre microservice avec lequel la connexion va s'effectuer
@Path("/candidat")
@Produces(MediaType.APPLICATION_JSON)

public interface CandidatProxy {
    
    //on met les méthodes qu'on veut appeler sur ce microservice distant
    @GET
    @Path("/ajouter")
    @Produces(MediaType.APPLICATION_JSON)//car on manipule un objet
    Candidat getCandidat();
}
    

