package com.interim;

import com.interim.entity.Candidature;
import com.interim.entity.Offre;
import com.interim.service.CandidatureService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/offres/{offerId}/candidature")

public class CandidatureResource {

    @Inject
    CandidatureService candidatureService;

    @POST
    @Transactional
    public Response createCandidature(@PathParam("offerId") Long offerId, Candidature candidature) {
        //Offre offre = entityManager.find(Offre.class, offerId);
        Candidature createdCandidature = candidatureService.createCandidature(offerId, candidature);
        return Response.ok(createdCandidature).build();
    }

    @GET
    public Response getCandidaturesByOfferId(@PathParam("offerId") Long offerId) {
        List<Candidature> candidatures = candidatureService.getCandidaturesByOfferId(offerId);
        return Response.ok(candidatures).build();
    }

    @GET
    @Path("/{id}")
    public Response getCandidatureById(@PathParam("id") Long id) {
        Candidature candidature = candidatureService.getCandidatureById(id);
        return Response.ok(candidature).build();
    }
}
