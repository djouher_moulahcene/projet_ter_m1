package com.interim.Mongodb;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.interim.entity.InterimAgency;
import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;



@ApplicationScoped
public class MongoRessource {

    private MongoClient mongoClient;
    private MongoDatabase database;

    @ConfigProperty(name ="mongodb+srv://ainasnouria:NourSamah05@cluster0.uklzauj.mongodb.net/?retryWrites=true&w=majority")
    
    String connectionString;

    @ConfigProperty(name ="InterimAgency")
    String databaseName;

    @PostConstruct
    void init() {
        ConnectionString connString = new ConnectionString(connectionString);
        mongoClient = MongoClients.create(connString);
        database = mongoClient.getDatabase(databaseName);
    }

    public MongoCollection<InterimAgency> getCollection(String collectionName) {
        return database.getCollection(collectionName, InterimAgency.class);
    }

    @PreDestroy
    void destroy() {
        mongoClient.close();
    }
}
