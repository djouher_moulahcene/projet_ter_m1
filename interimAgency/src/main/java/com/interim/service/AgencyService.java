package com.interim.service;


import com.interim.entity.InterimAgency;

import io.quarkus.mongodb.panache.PanacheMongoRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import org.bson.types.ObjectId;

import java.util.List;

@ApplicationScoped
public class AgencyService implements PanacheMongoRepositoryBase<InterimAgency,ObjectId>{

    @Transactional
    public InterimAgency createAgency(InterimAgency agency) {
        agency.persist();
        return agency;
    }

    public List<InterimAgency> getAllAgencies() {
        return InterimAgency.listAll();
    }

    public InterimAgency getAgencyById(String id) {
        return InterimAgency.findById(new ObjectId(id));
    }

    /*
     private String name;
    private String departement_name;
    private String subDepartementName;
    private String companyNationalId;

    private String contact1Name;
    private String contact1Email;
    private String contact2Name;
    private String contact2Email;

    private String address;
    private String city;
    private String country;
    private String code;
     */
    @Transactional
    public void updateAgency(String id, InterimAgency agency) {
        InterimAgency entity = agency.findById(new ObjectId(id));
        entity.setName(agency.getName());
        entity.setDepartementName(agency.getDepartementName());
        entity.setSubDepartementName(agency.getSubDepartementName());
        entity.setCompanyNationalId(agency.getCompanyNationalId());
        entity.setContact1Name(agency.getContact1Name());
        entity.setContact1Email(agency.getContact1Email());
        entity.setContact2Name(agency.getContact2Name());
        entity.setContact2Email(agency.getContact2Email());
        entity.setAddress(agency.getAddress());
        entity.setCity(agency.getCity());
        entity.setCode(agency.getCode());
    }

    @Transactional
    public void deleteAgency(String id) {
        InterimAgency.deleteById(new ObjectId(id));
    }

}