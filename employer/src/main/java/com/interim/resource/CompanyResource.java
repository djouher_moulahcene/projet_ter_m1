package com.interim.resource;


import com.interim.entity.Company;
import com.interim.service.CompanyService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/companies")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CompanyResource {

    @Inject
    CompanyService companyService;

    @GET
    public Response getAllCompanies() {
        List<Company> companies = companyService.getAllCompanies();
        return Response.ok(companies).build();
    }

    @GET
    @Path("/{id}")
    public Response getCompanyById(@PathParam("id") Long id) {
        Company company = companyService.getCompanyById(id);
        if (company != null) {
            return Response.ok(company).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    public Response createCompany(Company company) {
        Company newCompany = companyService.createCompany(company);
        return Response.ok(newCompany).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateCompany(@PathParam("id") Long id, Company company) {
        companyService.updateCompany(id, company);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteCompany(@PathParam("id") Long id) {
        companyService.deleteCompany(id);
        return Response.ok().build();
    }

}