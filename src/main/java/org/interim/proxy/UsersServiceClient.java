package org.interim.proxy;


import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.interim.Entity.Utilisateur1;
import javax.ws.rs.core.MediaType;
@Path("/users1")
@RegisterRestClient(configKey="users-service")
public interface UsersServiceClient {

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    void createUser(Utilisateur1 utilisateur);
}
