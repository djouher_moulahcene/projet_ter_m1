package org.interim.proxy;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(baseUri = "http://localhost/8081")//c'est le port de l'autre microservice avec lequel la connexion va s'effectuer
@Path("/candidat")
@Produces(MediaType.APPLICATION_JSON)
public interface CandidatServiceClient {

    @POST
    @Path("/ajouter")
    Candidat createCandidat(Candidat candidat);

}
