package org.interim.Ressource;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.interim.Entity.Utilisateur1;

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AuthResource {
   /*  @Inject
    private PasswordHasher passwordHasher;

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(UserDTO user) {
        Utilisateur1 dbUser = Utilisateur1.findByUsername(user.getPassword());
        if (dbUser == null) {
            throw new WebApplicationException("Invalid username/password", Response.Status.UNAUTHORIZED);
        }
        if (!passwordHasher.verify(dbUser.getMont(), user.getPassword().toCharArray())) {
            throw new WebApplicationException("Invalid username/password", Response.Status.UNAUTHORIZED);
        }
        String token = JWTUtil.generateToken(dbUser.getNom(), dbUser.getRole());
        return Response.ok(new TokenDTO(token)).build();
    }*/
    @POST
    @Transactional
    public Response authenticateUser(Utilisateur1 user) {
      Utilisateur1 existingUser = Utilisateur1.find("nom_utilisateur", user.getNom()).firstResult();
      if (existingUser != null && existingUser.getMont().equals(user.getMont())&&existingUser.getRole().equals(user.getRole())) {
        return Response.ok().build();
      } else {
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
    }
}

